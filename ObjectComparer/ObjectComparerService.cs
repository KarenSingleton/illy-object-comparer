﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectComparer
{
    public static class ObjectComparerService
    {
        public static IEnumerable<string> GetChanges(Object obj1, Object obj2)
        {
            if (obj1 == null)
                throw new ArgumentNullException(nameof(obj1));
            if (obj2 == null)
                throw new ArgumentNullException(nameof(obj2));
            if (obj1.GetType() != obj2.GetType())
                throw new ArgumentException("Both objects need to be of the same type");

            var properties = obj1.GetType().GetProperties();

            // loop over properties - check for differing values and add to collection
            foreach(var property in properties)
            {
                var obj1Property = property.GetValue(obj1);
                var obj2Property = property.GetValue(obj2);
                
                if (!obj1Property.Equals(obj2Property))
                    yield return $"{property.Name.PascalToTitleCase()} changed from '{obj1Property}' to '{obj2Property}'";
            }
        }        
    }
}
