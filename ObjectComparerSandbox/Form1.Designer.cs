﻿namespace ObjectComparerSandbox
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtDateProperty = new System.Windows.Forms.TextBox();
            this.txtStringProperty = new System.Windows.Forms.TextBox();
            this.txtIntegerProperty = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblDateProperty = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblStringProperty = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblIntegerProperty = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.outputChangesList = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel2);
            this.splitContainer1.Panel1.Controls.Add(this.panel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.outputChangesList);
            this.splitContainer1.Panel2.Controls.Add(this.button1);
            this.splitContainer1.Panel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.splitContainer1.Size = new System.Drawing.Size(745, 347);
            this.splitContainer1.SplitterDistance = 232;
            this.splitContainer1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtDateProperty);
            this.panel2.Controls.Add(this.txtStringProperty);
            this.panel2.Controls.Add(this.txtIntegerProperty);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Location = new System.Drawing.Point(465, 37);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(268, 187);
            this.panel2.TabIndex = 8;
            // 
            // txtDateProperty
            // 
            this.txtDateProperty.Location = new System.Drawing.Point(133, 135);
            this.txtDateProperty.Name = "txtDateProperty";
            this.txtDateProperty.Size = new System.Drawing.Size(100, 20);
            this.txtDateProperty.TabIndex = 11;
            // 
            // txtStringProperty
            // 
            this.txtStringProperty.Location = new System.Drawing.Point(133, 103);
            this.txtStringProperty.Name = "txtStringProperty";
            this.txtStringProperty.Size = new System.Drawing.Size(100, 20);
            this.txtStringProperty.TabIndex = 10;
            // 
            // txtIntegerProperty
            // 
            this.txtIntegerProperty.Location = new System.Drawing.Point(133, 75);
            this.txtIntegerProperty.Name = "txtIntegerProperty";
            this.txtIntegerProperty.Size = new System.Drawing.Size(100, 20);
            this.txtIntegerProperty.TabIndex = 9;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(37, 28);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Object 2";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(37, 78);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(81, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Integer property";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(130, 138);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(37, 138);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Date property";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(130, 106);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 13);
            this.label6.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(37, 106);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "String property";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblDateProperty);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.lblStringProperty);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.lblIntegerProperty);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(12, 37);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(281, 187);
            this.panel1.TabIndex = 0;
            // 
            // lblDateProperty
            // 
            this.lblDateProperty.AutoSize = true;
            this.lblDateProperty.Location = new System.Drawing.Point(130, 138);
            this.lblDateProperty.Name = "lblDateProperty";
            this.lblDateProperty.Size = new System.Drawing.Size(0, 13);
            this.lblDateProperty.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(37, 138);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Date property";
            // 
            // lblStringProperty
            // 
            this.lblStringProperty.AutoSize = true;
            this.lblStringProperty.Location = new System.Drawing.Point(130, 106);
            this.lblStringProperty.Name = "lblStringProperty";
            this.lblStringProperty.Size = new System.Drawing.Size(0, 13);
            this.lblStringProperty.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(37, 106);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "String property";
            // 
            // lblIntegerProperty
            // 
            this.lblIntegerProperty.AutoSize = true;
            this.lblIntegerProperty.Location = new System.Drawing.Point(130, 78);
            this.lblIntegerProperty.Name = "lblIntegerProperty";
            this.lblIntegerProperty.Size = new System.Drawing.Size(0, 13);
            this.lblIntegerProperty.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(37, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Integer property";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Object 1";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(52, 43);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(99, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "Get Changes";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // outputChangesList
            // 
            this.outputChangesList.Location = new System.Drawing.Point(212, 17);
            this.outputChangesList.Name = "outputChangesList";
            this.outputChangesList.Size = new System.Drawing.Size(394, 69);
            this.outputChangesList.TabIndex = 10;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(745, 347);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblDateProperty;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblStringProperty;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblIntegerProperty;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtDateProperty;
        private System.Windows.Forms.TextBox txtStringProperty;
        private System.Windows.Forms.TextBox txtIntegerProperty;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label outputChangesList;
    }
}

