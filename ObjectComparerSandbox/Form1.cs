﻿using ObjectComparer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ObjectComparerSandbox
{
    public partial class Form1 : Form
    {
        private SandboxObject Object1 { get; set; }
        private SandboxObject Object2 { get; set; }

        public Form1()
        {
            InitializeComponent();


            // initialise object 1 and 2, set the properties to be the same...
            Object1 = SandboxObject.CreateSandboxObject();
            Object2 = SandboxObject.CreateSandboxObject();

            lblIntegerProperty.Text = Object1.IntegerProprety.ToString();
            lblDateProperty.Text = Object1.DateProperty.ToString();
            lblStringProperty.Text = Object1.StringProperty;

            txtIntegerProperty.Text = Object2.IntegerProprety.ToString();
            txtDateProperty.Text = Object2.DateProperty.ToString();
            txtStringProperty.Text = Object2.StringProperty;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // invoke object comparer service - output the list of changes to the panel.
            BindData();
            var changes = ObjectComparerService.GetChanges(Object1, Object2);

            outputChangesList.Text = string.Join(Environment.NewLine, changes);
        }

        private void BindData()
        {
            // update object2's properties
            Object2.IntegerProprety = Int32.Parse(txtIntegerProperty.Text);
            Object2.DateProperty = DateTime.Parse(txtDateProperty.Text);
            Object2.StringProperty = txtStringProperty.Text;
        }
    }
}
