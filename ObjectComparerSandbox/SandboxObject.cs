﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectComparerSandbox
{
    public class SandboxObject
    {
        public int IntegerProprety { get; set; }
        public string StringProperty { get; set; }
        public DateTime DateProperty { get; set; }

        public static SandboxObject CreateSandboxObject()
        {
            return new SandboxObject{ IntegerProprety = 42, DateProperty = DateTime.Today, StringProperty="Jae Jae"};
        }
    }
}
